import javafx.util.Pair

fun main(args: Array<String>){

    //Other data types : Pairs and Triples
    val date = Triple("Manuela",3.14,14)

    val (name,designation,age) = date

    println("name: $name; designation: $designation; age: $age")

    print("x1: ${date.first}; y1: ${date.second}; z1: ${date.third}")

    val (x, _ ,z) = Triple(12,14,16)

    print("x: $x; z: $z")


    //KOTLIN IN ACTION ----> CHAPTER 2
    /*Some basics before get into classes >.<" */
    /**
     * unlike java, in kotlin you don't need to specify the type of your variable;
     * Kotlin can infer it
     * */

    val firstNumber = 13
    //is the same that
    val secondNumber: Int = 13
    //where you specified the type of your data

    /**
     * If a variable doesn't have an initializer, you need to specify its type explicity:
     * */

    val answer : Int
    answer = 13

    /**
     * A val variable must be initialized once during the execution of the block where it's defined
     * */
    val message: String
    if(canPerformedOperation()) {
        message = "Succes"
    } else{
        message = "Failed"
    }

    /**
     * Note that, even though a 'val' reference is itself immutable and can't be changed,
     * the object that it points to may be mutable
     * */
    val languages = arrayListOf("Java")
    languages.add("Kotlin")



}

/*page : 23*/

fun canPerformedOperation(): Boolean {
return true
}
